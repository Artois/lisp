package migl.lisp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LispParserOwnTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructWithNull() throws LispError {
		new LispParser(null);
	}
	
	@Test(expected = LispError.class)
	public void testMultiElementParser() throws LispError {
		LispParser lp = new LispParser("10 str");
		lp.parse();
	}
	
	@Test
	public void testExplode() {
		try {
			LispParser lp = new LispParser("(aze (10 5.8))");
			lp.parse();
		} catch (LispError e) {
			fail("Erreur pendant l'analyse");
		}
	}
	
	@Test
	public void testBadEndList() throws LispError {
		LispParser lp = new LispParser("(aze 158 8.6");
		assertFalse(lp.verify());
	}
	
	@Test
	public void testListandElementAfter() throws LispError {
		LispParser lp = new LispParser("(aze (aze rty)) aze");
		assertFalse(lp.verify());
	}
	
	@Test
	public void testGetExpression() {
		String expr = "(+ 4 8)";
		LispParser lp = new LispParser(expr);
		assertEquals(expr, lp.getExpression());
	}
	
	@Test
	public void testParserNotSet() {
		LispParser lp = new LispParser();
		assertEquals("", lp.getExpression());
	}
	
	@Test
	public void testMultipleParse() throws LispError {
		LispParser expected = new LispParser("(8 4)");
		LispParser lp = new LispParser();
		lp.setExpr("(8 4)");
		assertEquals(expected.parse(), lp.parse());
		lp.setExpr("(8 4)");
		assertEquals(expected.parse(), lp.parse());
	}

}
