package migl.lisp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class LispImplOwnTest {

	private LispImpl interpreter;

	@Before
	public void init() {
		interpreter = (LispImpl) LispFactory.makeIntepreter();
	}

	@Test
	public void testParse() throws LispError {
		LispParser expected = new LispParser("(+ (* 2 4) (- 2 2))");
		assertEquals(expected.parse(), interpreter.parse("(+ (* 2 4) (- 2 2))"));
	}
	
	@Test
	public void testEval() throws LispError {
		LispParser lp = new LispParser("(+ (* 2 4) (- 2 2))");
		Object lisp = lp.parse();
		LispEval expected = interpreter.getEval();
		expected.setLisp(lisp);
		assertEquals(expected.evaluate(), interpreter.evaluate(lisp));
		assertEquals(expected.evaluate(), interpreter.eval("(+ (* 2 4) (- 2 2))"));
	}
	
	@Test
	public void testGetParser() {
		assertTrue(interpreter.getParser().getClass() == LispParser.class);
	}
	
	@Test
	public void testGetEval() {
		assertTrue(interpreter.getEval().getClass() == LispEval.class);
	}
	
}
