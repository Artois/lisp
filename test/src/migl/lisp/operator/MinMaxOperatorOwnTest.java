package migl.lisp.operator;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import migl.lisp.Lisp;
import migl.lisp.LispElement;
import migl.lisp.LispError;
import migl.lisp.LispEval;
import migl.lisp.LispFactory;
import migl.util.ConsList;
import migl.util.ConsListFactory;

public class MinMaxOperatorOwnTest {

	private Lisp interpreter;

	@Before
	public void init() {
		interpreter = LispFactory.makeIntepreter();
	}
	
	@Test
	public void testMinOperator() throws LispError {
		//Commentaire gestion uniquement de deux variable car desactivé
		//assertEquals(LispElement.generate(8), interpreter.eval("(min 8)"));
		//assertEquals(LispElement.generate(8.4), interpreter.eval("(min 8.4)"));
		assertEquals(LispElement.generate(5), interpreter.eval("(min 8 5)"));
		//assertEquals(LispElement.generate(2), interpreter.eval("(min 8 5 4 6 2)"));
		assertEquals(LispElement.generate(5.0), interpreter.eval("(min 8 5.0)"));
		//assertEquals(LispElement.generate(4.2), interpreter.eval("(min 8 5.0 8 4.2)"));
		assertEquals(LispElement.generate(5.0), interpreter.eval("(min 8.0 5.0)"));
	}
	
	@Test(expected = LispError.class)
	public void testIncorrectNumberArgumentMin() throws LispError {
		interpreter.eval("(min)");
	}
	
	@Test
	public void testMaxOperator() throws LispError {
		//Commentaire gestion de plus de deux variable car desactivé
		//assertEquals(LispElement.generate(8), interpreter.eval("(max 8)"));
		//assertEquals(LispElement.generate(8.4), interpreter.eval("(max 8.4)"));
		assertEquals(LispElement.generate(8), interpreter.eval("(max 8 5)"));
		//assertEquals(LispElement.generate(8), interpreter.eval("(max 8 5 4 6 2)"));
		assertEquals(LispElement.generate(8.0), interpreter.eval("(max 8 5.0)"));
		//assertEquals(LispElement.generate(8.4), interpreter.eval("(max 8.4 5.0 8 4.2)"));
		assertEquals(LispElement.generate(8.0), interpreter.eval("(max 8.0 5.0)"));
	}
	
	@Test(expected = LispError.class)
	public void testIncorrectNumberArgumentMax() throws LispError {
		interpreter.eval("(max)");
	}
	
	@Test(expected = LispError.class)
	public void testMinMaxOperatorException1() throws LispError {
		LispOperator comp = new MinMaxOperator();
		ConsList<Object> cl = ConsListFactory.nil();
		cl = cl.prepend(new BigInteger("8"));
		cl = cl.prepend(4.2);
		comp.apply(new LispEval(interpreter), "_", cl);
	}
	
	@Test(expected = LispError.class)
	public void testMinMaxOperatorException2() throws LispError {
		LispOperator minMax = new MinMaxOperator();
		ConsList<Object> cl = ConsListFactory.nil();
		cl = cl.prepend(new BigInteger("8"));
		cl = cl.prepend(new BigInteger("4"));
		minMax.apply(new LispEval(interpreter), "_", cl);
	}
	
}
