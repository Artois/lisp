package migl.lisp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LispErrorTest {

	@Test
	public void testLispErrorMessageCause() {
		Throwable cause = new IllegalArgumentException();
		try {
			throw new LispError("Erreur test", cause);
		} catch (LispError ex) {
			assertEquals("Erreur test", ex.getMessage());
			assertEquals(cause, ex.getCause());
		}
	}
	
	@Test
	public void testLispErrorMessage() {
		try {
			throw new LispError("Erreur test");
		} catch (LispError ex) {
			assertEquals("Erreur test", ex.getMessage());
		}
	}
	
	@Test
	public void testLispErrorCause() {
		Throwable cause = new IllegalArgumentException();
		try {
			throw new LispError(cause);
		} catch (LispError ex) {
			assertEquals(cause, ex.getCause());
		}
	}
	
}
