package migl.lisp;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class ReplTest {

    private ByteArrayOutputStream myout;

    @Before
    public void setUp() {
        myout = new ByteArrayOutputStream();
        System.setOut(new PrintStream(myout));
    }

    @Test
    public void test() {
        String sep = System.lineSeparator();
        ByteArrayInputStream myin = new ByteArrayInputStream(("(+ 1 2)" + sep + "quit" + sep).getBytes());
        System.setIn(myin);
        REPL.main(new String[] {});
        assertEquals("My super own Lisp/Scheme interpreter 2019" + sep
                + "Enter a valid Lisp expression followed by Enter. type 'quit' to exit." + sep + "> " + "3" + sep
                + "> " + "Bye." + sep, myout.toString());

    }

}
