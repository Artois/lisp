package migl.lisp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class LispBooleanOwnTest {
	
	@Test
	public void testLispBooleanValue(){
		assertTrue(LispBoolean.TRUE.value());
		assertFalse(LispBoolean.FALSE.value());
	}
	
	@Test
	public void testLispBooleanToString() {
		assertEquals("#t", LispBoolean.TRUE.toString());
		assertEquals("#f", LispBoolean.FALSE.toString());
	}
	
	@Test
	public void testLispBooleanValueOfBoolean() {
		assertEquals(LispBoolean.TRUE, LispBoolean.valueOf(true));
		assertEquals(LispBoolean.FALSE, LispBoolean.valueOf(false));
	}
	
	@Test
	public void testLispBooleanHashCode() {
		assertEquals(new Boolean(true).hashCode(), LispBoolean.TRUE.hashCode());
		assertEquals(new Boolean(false).hashCode(), LispBoolean.FALSE.hashCode());
		Map<LispBoolean, String> map = new HashMap<>();
		map.put(LispBoolean.TRUE, "#t");
		map.put(LispBoolean.FALSE, "#f");
		assertEquals("#t", map.get(LispBoolean.TRUE));
		assertEquals("#f", map.get(LispBoolean.FALSE));
	}
	
	@Test
	public void testLisbBooleanEquals() {
		assertTrue(LispBoolean.TRUE.equals(LispBoolean.TRUE));
		assertFalse(LispBoolean.TRUE.equals(LispBoolean.FALSE));
		assertTrue(LispBoolean.FALSE.equals(LispBoolean.FALSE));
		assertTrue(LispBoolean.TRUE.equals(LispBoolean.valueOf(true)));
		assertTrue(LispBoolean.FALSE.equals(LispBoolean.valueOf(false)));
		assertTrue(LispBoolean.TRUE.equals(LispBoolean.valueOf("#t")));
		assertTrue(LispBoolean.FALSE.equals(LispBoolean.valueOf("#f")));
		assertFalse(LispBoolean.TRUE.equals(true));
		assertFalse(LispBoolean.FALSE.equals(false));
	}

}
