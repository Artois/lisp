package migl.lisp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


import org.junit.Before;
import org.junit.Test;

public class LispFactoryOwnTest {
	
	private Lisp firstInstance;

	@Before
	public void init() {
		firstInstance = LispFactory.getInterpreter();
		try {
			firstInstance.eval("(define var 8)");
		} catch (LispError e) {
			fail();
		}
	}

	@Test(expected = LispError.class)
	public void testMakeInterpreter() throws LispError {
		Lisp l = LispFactory.makeIntepreter();
		assertTrue(firstInstance != l);
		l.eval("var");
	}
	
	@Test
	public void testGetInterpreter() throws LispError {
		Lisp l = LispFactory.getInterpreter();
		assertTrue(firstInstance == l);
		assertEquals(LispElement.generate(8), l.eval("var"));
	}
	
	@Test
	public void testDefineDontUseTheLastInterpreter1() throws LispError {
		assertEquals(LispElement.generate(8), firstInstance.eval("var"));
		LispFactory.makeIntepreter();
		assertEquals(LispElement.generate(8), firstInstance.eval("var"));
	}
	
	@Test
	public void testDefineDontUseTheLastInterpreter2() throws LispError {
		firstInstance.eval("(define f (lambda (x) (* x 2)))");
		assertEquals(LispElement.generate(8), firstInstance.eval("(f 4)"));
		LispFactory.makeIntepreter();
		assertEquals(LispElement.generate(8), firstInstance.eval("(f 4)"));
	}
	
	@Test
	public void testDefineDontUseTheLastInterpreter3() throws LispError {
		firstInstance.eval("(define func (lambda (x) (+ x var)))");
		assertEquals(LispElement.generate(12), firstInstance.eval("(func 4)"));
		LispFactory.makeIntepreter();
		assertEquals(LispElement.generate(12), firstInstance.eval("(func 4)"));
	}
	
	@Test
	public void testLispElementCacheIsClear() {
		LispElement.generate(8.4);
		assertTrue(LispElement.getCacheSize() > 0);
		LispFactory.makeIntepreter();
		assertEquals(0, LispElement.getCacheSize());
	}
	
}
