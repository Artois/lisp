package migl.lisp;

import org.junit.Before;

public abstract class AbstractTDD {

    protected Lisp lisp;

    @Before
    public void init() {
        lisp = LispFactory.makeIntepreter();
    }
}
