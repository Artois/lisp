package migl.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConsOwnTest {

	@Test
	public void testEquals() {
		Cons<String, String> c1 = new Cons<>(null, "str");
		Cons<String, String> c2 = new Cons<>("not null", "str");
		assertFalse(c1.equals(c2));
	}

}
