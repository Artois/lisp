Some basic mathematical operations

Narrative:
Examples contributed by Alexandre Magniez to improve tests code coverage.

Scenario: built-in operators

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(max 1.2 2.2)|	 2.2|
|(min 1.2 2.2)|	1.2|
|(+ 1.0 2.0) |	3.0|
|(- 2.0 1.0)|	1.0|
|(* 2.0 1.0)| 	2.0|
|(/ 2.0 1.0)| 	2.0|
|(< 1 2)	|    #t|
|(< 1.2 2.2)|    #t|
|(> 1 2)	|	 #f|
|(> 1.2 2.2)|	 #f|
|(abs -5.0)	|	5.0|
|(pow 1.0 2.0)|	1.0|



Scenario: basic error messages

Given a lisp interpreter
When the expression entered is <expression>
Then the result should display the error message <result>

Examples:

|expression  |result|
|(/ 1 2 3)|Invalid number of operands|
|(abs 1 2)|Invalid number of operands|
|(pow 1 2 3)|Invalid number of operands|
