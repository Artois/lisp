package migl.bdd;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import migl.lisp.Lisp;
import migl.lisp.LispError;
import migl.lisp.LispFactory;

public class LispEval {

    private Lisp lisp;

    private Object eval;
    private LispError ex;

    @Given("a lisp interpreter")
    public void newInterpreter() {
        lisp = LispFactory.makeIntepreter();
    }

    @When("the expression entered is $expression")
    public void eval(String expression) {
        try {
            eval = lisp.eval(expression);
            ex = null;
        } catch (LispError e) {
            ex = e;
        }
    }

    @Then("the result should be $expectedValue")
    public void checkValue(String expectedValue) {
        assertThat("An exception launched?", ex, is(nullValue()));
        assertThat(eval.toString(), equalTo(expectedValue));
    }

    @Then("the result should display the error message $message")
    public void checkError(String message) {
        assertThat("No exception launched?", ex, is(notNullValue()));
        assertThat(ex.getMessage(), equalTo(message));
    }
}
