Some basic mathematical operations

Narrative:
We need to check that our interpreter performs the
same operations as jscheme (at least on classical
lisp/scheme operations). It may differ on some 
specific but documented cases.

Scenario: built-in operators

Given jscheme and a lisp interpreter

When the expression evaluated is <expression>
Then the results should be identical

Examples:

|expression|
|(+ 1 1)   |
|(+ 1 1 1) |
|(+ 1 1 1 2) |
|(- 1 3)   |
|(* 1 3)   |
|(* 1 3 5) |
|(* 1 3 5 7) |
|(/ 3 2)   |
|(max 2 3) |

Scenario: cons

Given jscheme and a lisp interpreter

When the expression evaluated is <expression>
Then the results should be identical

Examples:

|expression|
|(cons 1 2)   |
|(cons 1 ()) |
|(cons () 1) |
|(cons () ())   |
|(cons (cons 1 2)  3) |
|(cons 1 (cons 2 ())) |