The final touch to our interpreter

Narrative:

In order to have a fully working interpreter, 

as a casual lisp user

I want to better manipulate lists, and express functions (lambda).

Scenario: cons and list operations

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(cons 1 2)  | (1 . 2)|
|(car (cons 1 2))| 1  |
|(cdr (cons 1 2))| 2  |
|(cons 1 (cons 2 ())) | (1 2)   |
|(car (cons 1 (cons 2 ()))) | 1 |
|(cdr (cons 1 (cons 2 ()))) | (2) |
|(list 1 2 3)   |  (1 2 3)   |

Scenario: more complex functions (lambdas)

Given a lisp interpreter

When the expression entered is (define twice (lambda (x) (* 2 x)))
Then the result should be lambda (x) (* 2 x)

When the expression entered is (define strange (lambda (f x) (f (f x))))
Then the result should be lambda (f x) (f (f x))

When the expression entered is (strange twice 10)
Then the result should be 40

Scenario: recursive functions - factorial

Given a lisp interpreter

When the expression entered is (define fact (lambda (n) (if (<= n 1) 1 (* n (fact (- n 1))))))
Then the result should be lambda (n) (if (<= n 1) 1 (* n (fact (- n 1))))

When the expression entered is (fact 0)
Then the result should be 1

When the expression entered is (fact 1)
Then the result should be 1

When the expression entered is (fact 2)
Then the result should be 2

When the expression entered is (fact 3)
Then the result should be 6

When the expression entered is (fact 10)
Then the result should be 3628800

When the expression entered is (fact 100)
Then the result should be 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000

Scenario: recursive functions - fibonacci

Given a lisp interpreter

When the expression entered is (define fib (lambda (n) (if (< n 2) 1 (+ (fib (- n 1)) (fib (- n 2))))))
Then the result should be lambda (n) (if (< n 2) 1 (+ (fib (- n 1)) (fib (- n 2))))

When the expression entered is (define range (lambda (a b) (if (= a b) (list) (cons a (range (+ a 1) b)))))
Then the result should be lambda (a b) (if (= a b) (list) (cons a (range (+ a 1) b)))

When the expression entered is (range 0 10)
Then the result should be (0 1 2 3 4 5 6 7 8 9)

When the expression entered is (map fib (range 0 10))
Then the result should be (1 1 2 3 5 8 13 21 34 55)

When the expression entered is (map fib (range 0 20))
Then the result should be (1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765)

Scenario: back to LCPF

Given a lisp interpreter

When the expression entered is (define f (lambda (x) (lambda (y) (* x y))))
Then the result should be lambda (x) (lambda (y) (* x y))

When the expression entered is ((f 10) 20)
Then the result should be 200

Scenario: some potential error

Given a lisp interpreter

When the expression entered is (define f (lambda (+ 2 3)))
Then the result should display the error message Invalid number of operands

Scenario: some students are picky about testing

Given a lisp interpreter

When the expression entered is <expression>
Then the result should display the error message <result>

Examples:

|expression|result|
|(define car 20)|car is not a valid identifier|
|(define cdr 20)|cdr is not a valid identifier|
|(define list 20)|list is not a valid identifier|

