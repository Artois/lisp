Some basic mathematical operations

Narrative:
Our lisp interpreter should be able to 
perform basic maths. It will rely on the
methods found in java.lang.Math for functions
like cos, sin, abs, etc.

Scenario: built-in operators

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(+ 1 1)     |  2   |
|(+ 1 2 3 4) | 10   |
|(- 5)       | -5   |
|(- 1 3)     | -2   |
|(* 1 3)     |  3   |
|(* 1 3 5 7) |105   |
|(/ 3 2)     |  1   |
|(max 2 3)   |  3   |

Scenario: Division by zero

Given a lisp interpreter
When the expression entered is (/ 3 0)
Then the result should display the error message Division by zero

Scenario: operands must be evaluated

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(+ (* 2 3) (- 8 5))  |  9   |
|(* (+ 1 (* 2 3) 4) 9) | 99   |
|(- (* 2 3) (* 3 4))| -6|
|(/ (* 2 7) (* 4 5.0))|0.7|
|(if (or (> 4 5) (< 4 5)) (+ 2 3) (- 3 2))| 5   |
|(or (= (+ 2 3) 5 (/ 10 2)) (> 5 0))|#t|
|(and (> (+ 2 3) (- 9 8)) (= (* 3 3) (+ 3 6) (/ 27 3) (- 12 3)))|#t|
|(not (> 26 56))|#t|

Scenario: java.lang.Math functions

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression|result|
|(abs (- 8))  |8 |
|(abs (- 8.0))|8.0 |
|(cbrt 8.0)|2.0|
|(ceil 8.45)|9.0|
|(floor 8.45)|8.0|
|(log10 100)|2.0|
|(cos 0)   |1.0   |
|(pow 2 3) |8.0   |
|(min 2 3) |2     |
|(min 2.0 3.0)|2.0|
|(max 2 3) |3     |
|(max 2.0 3.0)|3.0|
|(rint 8.45)|8.0|
|(rint 8.55)|9.0|
|(round 8.45)|8|
|(round 8.55)|9|
|(signum 5.9)|1.0|
|(signum (- 5.9))|-1.0|
|(signum 0.0)|0.0|

Scenario: Wrong number of operands for Math methods poke M. Valembois

Given a lisp interpreter
When the expression entered is <expression>
Then the result should display the error message Invalid number of operands

Examples:

|expression|
|(abs 8 9)  |
|(abs)|
|(abs 8.0 9.0)|
|(cbrt 8.0 16.0)|
|(cbrt)|
|(ceil 8.45 9.1)|
|(ceil)|
|(floor 8.45 9.12)|
|(floor)|
|(log10 100 1000)|
|(log10)|
|(cos 0 90)|
|(cos)|
|(pow 2 3 4)|
|(pow)|
|(min 2 3 4) |
|(min)|
|(min 2.0 3.0 4.0)|
|(max 2 3 4) |
|(max)|
|(max 2.0 3.0 4.0)|
|(rint 8.45 9.45)|
|(rint)|
|(round 8.45 9.55)|
|(round)|
|(signum 5.9 (- 7.8))|
|(signum)|
