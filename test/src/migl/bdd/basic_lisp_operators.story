Some additional lisp operations

Narrative:
Our lisp interpreter should be able to 
perform those lisp operations. 

Scenario: built-in lisp operators

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|

|(quote (+ 1 2))|(+ 1 2)|
|(if #t 3 4)|3|
|(if #f 3 4)|4|
|(if (> 10 20) (+ 1 1) (+ 3 3))|6|
|(if (> 1 2) 3 5)|5|
|(cons 1 2)|(1 . 2)|
|(cons 1 ())|(1)|
|(cons () 1)|(() . 1)|
|(cons () ())|(())|
|(cons (cons 1 2)  3)|((1 . 2) . 3)|
|(cons 1 (cons 2 ())) |(1 2)|
|(and #f (+ 2 3))|#f|
|(or #t (+ 2 3))|#t|

Scenario: additional tests from V. Valembois

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|

|(= 2 2 2)|#t|
|(= 2)|#t|
|(< 1)|#t|
|(> 1)|#t|
|(< 1 2 3)|#t|
|(> 1 2 3)|#f|
|(or)|#f|
|(or #t)|#t|
|(or #f)|#f|
|(or #f #f #t)|#t|
|(or #f #f #f #f)|#f|
|(and)|#t|
|(and #t)|#t|
|(and #f)|#f|
|(and #t #t #f)|#f|
|(and #t #t #t #t)|#t|
|(and #t #t #t #f)|#f|

Scenario: additional tests from M. Miceli

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(= 2.0 2.0)|#t|
|(= 2.0 3.0)|#f|
|(<= 3.0 2.0)|#f|
|(<= 2.0 3.0)|#t|
|(<= 3.0 2)|#f|
|(<= 2 3.0)|#t|
|(>= 3.0 2.0)|#t|
|(>= 2.0 3.0)|#f|
|(>= 3 2.0)|#t|
|(>= 2.0 3)|#f|


Scenario: additional tests from A. Franco

 Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(< 1 2 1.5)|#f|
|(= 1 1 1 3)|#f|
|(> 2 1 1.5)|#f|

Scenario: basic error messages

Given a lisp interpreter
When the expression entered is <expression>
Then the result should display the error message <result>

Examples:

|expression  |result|
|(cons 1)|Invalid number of operands|
|(cons)|Invalid number of operands|
|(quote)|Invalid number of operands|
|(quote (+ 1 2) (+ 1 2))|Invalid number of operands|
|(+ - 2)|Not a number|
|(- 2 3 4)|Invalid number of operands|
|(/)|Invalid number of operands|
|(/ 2)|Invalid number of operands|
|(/ 2 4 6)|Invalid number of operands|
|(if)|Invalid number of operands|
|(if #t)|Invalid number of operands|
|(if #t 3)|Invalid number of operands|
|(if #t 3 4 5)|Invalid number of operands|
|(if (+ 1 2) 3 4)|Not a Boolean|
|(not (+ 2 3))|Not a Boolean|
|(and (+ 2 3) #t)|Not a Boolean|
|(and #t (+ 2 3))|Not a Boolean|
|(or (+ 2 3) #f)|Not a Boolean|
|(or #f (+ 2 3))|Not a Boolean|
|(<)|Invalid number of operands|
|(>)|Invalid number of operands|
|(not)|Invalid number of operands|
|(not #t #t)|Invalid number of operands|

Scenario: Division by zero

Given a lisp interpreter
When the expression entered is (/ 3 0)
Then the result should display the error message Division by zero
