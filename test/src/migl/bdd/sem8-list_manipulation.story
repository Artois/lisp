Some list related operations

Narrative:
Most of the features of the interpreter rely
on the use of cons and lists. As such, it is important
to test thoroughly those features.

Scenario: cons operator

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression|result|
|(cons 1 2)   |(1 . 2)     |
|(cons 1 nil) |(1)   |
|(cons nil 1) |(() . 1)     |
|(cons nil nil)   |(())     |
|(cons (cons 1 2)  3) |((1 . 2) . 3)   |
|(cons 1 (cons 2 nil)) |(1 2)   |
