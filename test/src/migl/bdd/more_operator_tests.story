Some additional mathematical operations

Narrative:
Our lisp interpreter should be able to 
perform those maths operations. 

Scenario: built-in operators

Given a lisp interpreter

When the expression entered is <expression>
Then the result should be <result>

Examples:

|expression  |result|
|(- (+ 2 3))|-5|
|(- 9 20)|-11|
|(- (+ (* 2 3) (/ 9 3)) 20)|-11|
|(- 9 (* 4 5))|-11|
|(- (* 4 5) 9)|11|
|(- (+ (* 2 3) (/ 9 3)) (* 4 5))|-11|