# Projet interpréteur lisp

[![Qualité](https://forge.univ-artois.fr/sonar67/api/badges/gate?key=arthur_brandao%3A2019%3AMyLisp)](https://forge.univ-artois.fr/sonar67/dashboard/index/arthur_brandao%3A2019%3AMyLisp)
[![Tests réussis](https://forge.univ-artois.fr/sonar67/api/badges/measure?key=arthur_brandao%3A2019%3AMyLisp&metric=test_success_density)](https://forge.univ-artois.fr/sonar67/dashboard/index/arthur_brandao%3A2019%3AMyLisp)
[![Couverture de code](https://forge.univ-artois.fr/sonar67/api/badges/measure?key=arthur_brandao%3A2019%3AMyLisp&metric=coverage)](https://forge.univ-artois.fr/sonar67/dashboard/index/arthur_brandao%3A2019%3AMyLisp)

[*Voir sur SonarQube*](https://forge.univ-artois.fr/sonar67/dashboard?id=arthur_brandao%3A2019%3AMyLisp)

La page [Lisp sur wikipedia](https://en.wikipedia.org/wiki/Lisp_%28programming_language%29) et la page [Scheme sur wikipedia](https://en.wikipedia.org/wiki/Scheme_%28programming_language%29) fournissent l'historique et les fonctionnalités du langage.

Le comportement de l'interpréteur (le tests) sont validés à l'aide de l'interpréteur [JScheme](http://jscheme.sourceforge.net/jscheme/main.html).

Le nombre de tests augmentera au cours du semestre jusqu'à obtenir un interpréteur pleinement fonctionnel.

Ce projet est basé sur le travail de [Peter Norvig en Python](http://norvig.com/lispy.html).

Peter Norvig a aussi proposé une version [Java de son interpréteur](http://norvig.com/jscheme.html). Cette version a été conçue à la naissance du langage, et ne peut pas être considérée comme une conception oriénté objet d'un interpréteur Lisp.

Plus récemment, [une version Java a été proposée par Remy Forax](https://forax.github.io/2014-06-01-e733e6af6114eff55149-lispy_in_java.html). 
Si cette solution utilise des concepts avancés et récents de Java (lambdas et streams), le code reste proche de la version python, donc pas vraiment orienté objet.

On pourrait imaginer, dans un futur proche, passer de la réalisation d'un simple interpréteur à [un système d'exploitation complet](http://metamodular.com/lispos.pdf).

## Pour déclarer ce projet comme source `upstream`

Il suffit de déclarer une fois ce projet comme dépôt distant de votre fork :

```shell
$ git remote add upstream https://forge.univ-artois.fr/m1-2018-2019/TDD2019IMPL.git
```

Ensuite, à chaque mise à jour de ce projet, vous pouvez mettre à jour votre fork
à l'aide des commandes suivantes :

```shell
$ git pull upstream master
$ git push
```

## Si vous ne voyez plus vos tests dans votre projet

Il suffit de rajouter le projet de tests comme un sous module du projet :

```shell
$ git submodule add https://forge.univ-artois.fr/m1-2018-2019/TDD2019TESTS.git
# remplacer le chemin absolu https://forge.univ-artois.fr/m1-2018-2019/TDD2019TESTS.git 
# par ../../m1-2018-2019/TDD2019TESTS.git dans le fichier `.gitmodules`.
$ git commit -m "Les tests sont de retour"
$ git push
```

## Commandes submodule (Lien vers les git des tests)

Pour initialiser les submodules :

```shell
$ git submodule update --init
```

Pour initialiser un submodule précis :

```shell
$ git submodule update --init <nom_dossier_submodule>
```

Pour mettre à jour les submodules :

```shell
$ git submodule update --remote
```

Pour mettre à jour un submodule précis :

```shell
$ git submodule update --remote <nom_dossier_submodule>
```



## Note à propos de PiTest

Avec la configuration par defaut du build.xml :

```xml
<!-- ligne 114 -->
<target name="m1" description="Verification des projets de TDD2019" depends="clean,build,tests,mutationCoverage" />

<!-- Define the SonarQube target -->
<target name="sonar" depends="m1" description="Analyse le code avec SonarQube">
```

La commande `ant -Detudiant=prenom_nom sonar` (qui est utilisé pour valider le code dans l'intégration continue de Gitlab), ne va pas lancer les tests sonar si tous les tests unitaires ne passent pas (ce qui peut être gênant). Ceci est causé par la dépendance qu'a sonar vers m1 et m1 vers mutationCoverage. Si tous les tests unitaires ne sont pas passés alors PiTest (mutationCoverage) va échouer, hors comme sa dépendance échoue, m1 échoue. Donc sonar ne se lance pas car sa dépendance à échoué.

Pour résoudre le problème j'ai déplacé le test de mutation. Il n'est plus exécuté en tant que dépendance de m1, mais en tant que subant. Ce qui permet en cas d'erreur de ne pas faire échouer m1 et donc permettre de lancer sonar. Le nouveau code est le suivant.

```xml
<!-- ligne 114 -->
<target name="m1" description="Verification des projets de TDD2019" depends="clean,build,tests">
	<subant failonerror="false" target="mutationCoverage">
		<fileset dir="." includes="build.xml" />
	</subant>
</target>

<!-- Define the SonarQube target -->
<target name="sonar" depends="m1" description="Analyse le code avec SonarQube">
```

