#!/bin/bash

if [ ! -d "TDD2019TESTS" ]; then
   echo "Adding main public test repository"
   git submodule add --force https://forge.univ-artois.fr/m1-2018-2019/TDD2019TESTS.git 
fi

if [ ! -d "TDD2019HIDDENTESTS" ]; then
   echo "Adding main hidden test repository"
   git submodule add --force https://forge.univ-artois.fr/root/TDD2019HIDDENTESTS.git 
fi

if [ ! -d "TDD2019OWNTESTS" ]; then
   echo "Adding own test repository for $1"
   git submodule add --force https://forge.univ-artois.fr/$1/TDD2019OWNTESTS.git 
fi

# git checkout HEAD .gitmodules
