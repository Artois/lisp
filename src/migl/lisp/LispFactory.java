package migl.lisp;


/**
 * Simple factory to access the interpreter implementation.
 * 
 * @author leberre
 *
 */
public class LispFactory {
	
	private static Lisp lastInterpreter;

    private LispFactory() {
        // do nothing
    }

    /**
     * Create new instance of the interpreter.
     * 
     * @return a new lisp interpreter.
     */
    public static Lisp makeIntepreter() {
    	LispElement.clear();
    	return lastInterpreter = new LispImpl();
    }
    
    /**
     * Get last instance of the interpreter.
     * 
     * @return a new lisp interpreter.
     */
    public static Lisp getInterpreter() {
    	if(lastInterpreter == null) {
    		lastInterpreter = new LispImpl();
    	}
        return lastInterpreter;
    }
}
