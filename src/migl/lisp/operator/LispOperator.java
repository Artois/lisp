package migl.lisp.operator;

import migl.lisp.LispElement;
import migl.lisp.LispError;
import migl.lisp.LispEval;
import migl.util.ConsList;

/**
 * Implementation d'un operateur lisp
 * 
 * @author Arthur Brandao
 */
@FunctionalInterface
public interface LispOperator {

	/**
	 * Applique l'operateur lisp sur une liste
	 * @param L'instance à utiliser pour évaluer l'expression
	 * @param operator Symbole de l'operateur
	 * @param lisp Liste d'element à traiter sans le symbole de l'operateur
	 * @return
	 * @throws LispError
	 */
	LispElement apply(LispEval eval, String operator, ConsList<Object> lisp) throws LispError;
	
}
