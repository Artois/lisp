package migl.lisp;

public class LispImpl implements Lisp {
	
	private final LispParser parser = new LispParser();
	
	private final LispEval eval = new LispEval(this);
	
	@Override
	public Object parse(String expr) throws LispError {
		this.parser.setExpr(expr);
		if(!this.parser.verify()) {
			throw new LispError("Invalid Format");
		}
		return this.parser.parse();
	}

	@Override
	public Object evaluate(Object lisp) throws LispError {
		this.eval.setLisp(lisp);
		return this.eval.evaluate();
	}
	
	/**
	 * Retourne l'instance utilisée pour analyser les epxressions lisp
	 * @return
	 */
	public LispParser getParser() {
		return this.parser;
	}
	
	/**
	 * Retourne l'instance utilisée pour évaluer les epxressions lisp
	 * @return
	 */
	public LispEval getEval() {
		return this.eval;
	}

}
