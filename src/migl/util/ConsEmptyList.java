package migl.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class ConsEmptyList<E> implements ConsList<E> {

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			@Override
			public boolean hasNext() {
				return false;
			}

			@Override
			public E next() {
				throw new NoSuchElementException("Liste vide");
			}
		};
	}

	@Override
	public ConsList<E> prepend(E e) {
		return new ConsListImpl<>(e, this);
	}

	@Override
	public ConsList<E> append(E e) {
		return new ConsListImpl<>(e, this);
	}

	@Override
	public boolean isEmpty() {
		//Toujours vide
		return true;
	}

	@Override
	public E car() {
		throw new NoSuchElementException("Liste vide");
	}

	@Override
	public ConsList<E> cdr() {
		return new ConsEmptyList<>();
	}

	@Override
	public int size() {
		//Toujours vide donc 0 element
		return 0;
	}

	@Override
	public <T> ConsList<T> map(Function<E, T> f) {
		return new ConsEmptyList<>();
	}
	
	@Override
	public String toString() {
		return "()";
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof ConsEmptyList<?>);
	}

}
